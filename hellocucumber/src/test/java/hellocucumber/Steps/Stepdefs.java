package hellocucumber.Steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import hellocucumber.Base.BaseUtil;
import hellocucumber.HomePage;
import org.junit.Assert;
import org.openqa.selenium.By;

public class Stepdefs extends BaseUtil {

    HomePage homepage;

    public Stepdefs(BaseUtil base) {
        driver = base.driver;
        homepage = new HomePage(driver);
    }


    @Given("^Navigate to the web page Sign In$")
    public void navigate_To_The_Web_Page_Sign_In() {
        driver.get("https://www.namecheap.com/");
    }

    @And("Enter ([^\"]*) and ([^\"]*)")
    public void enterUsernameAndPassword(String username, String password) {
        homepage.inputLoginDetails(username, password);
    }

    @And("^Click Sign In$")
    public void ClickSignIn() {
        homepage.signIn();
    }

    @Then("^User page should load$")
    public void userPageShouldLoad() {
        String userPage = driver.getCurrentUrl();
        Assert.assertTrue(userPage.equals("https://ap.www.namecheap.com/"));

    }

    @Given("^Navigate to the web page$")
    public void navigateToTheWebPage() {
        driver.get("https://www.namecheap.com/");
    }

    @Then("Locate header bar")
    public void locateSignInButtonInTheLeftTopCornerOfThePage() {
        Assert.assertTrue(driver.findElement(By.xpath("//div[@class='gb-top-block__nav gb-col']")).isDisplayed());
    }

    @Then("Check if navigation bar is present in the header of the page")
    public void checkIfNavigationBarIsPresentInTheHeaderOfThePage() {
        Assert.assertTrue(driver.findElement(By.xpath("//div[@class='gb-navbar__drop']")).isDisplayed());
    }

    @And("Hover over each navigation option")
    public void hoverOverEachNavigationOption() {

    }

    @Then("Make sure each option has a dropdown menu")
    public void makeSureEachOptionHasADropdownMenu() {
        System.out.println("Check dropdowns");
    }

    @And("Hover over dropdown list options")
    public void hoverOverDropdownListOptions() {
        System.out.println("Hover dropdowns");
    }

    @Then("Make sure each dropdown window has blue highlight on hover")
    public void makeSureEachDropdownWindowHasBlueHighlightOnHover() {
        System.out.println("Check window background");
    }

    @Given("Open the home page")
    public void openTheHomePage() {
        System.out.println("Land on the home page");
    }

    @Given("Locate the home page")
    public void locateTheHomePage() {
        System.out.println("Navigate to the page");
    }
}
