package hellocucumber.Steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import hellocucumber.Base.BaseUtil;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Hooks extends BaseUtil {

    private BaseUtil base;

    public Hooks(BaseUtil base) {
        this.base = base;
    }

    @Before
    public void initializeTestRun() {
        System.out.println("Opening browser");
        System.setProperty("webdriver.chrome.driver", "/Users/marina/Documents/QA/cucumber/hellocucumber/src/test/resources/chromedriver");
        base.driver = new ChromeDriver();
        base.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        base.driver.manage().window().maximize();
    }

    @After
    public void quitRunning() {
        System.out.println("Closing browser");
        base.driver.close();
    }
}
