Feature: Navigation Bar Feature
  This feature checks that navigation bar on home page meets requirements

  Scenario: Check that navigation bar is being loaded as you open the home page
    Given Navigate to the web page
    Then Check if navigation bar is present in the header of the page

  Scenario: Check that each navigation bar option has a dropdown on hover
    Given Open the home page
    And Hover over each navigation option
    Then Make sure each option has a dropdown menu

  Scenario: Check that each dropdown window has blue highlight on hover
    Given Locate the home page
    And Hover over each navigation option
    And Hover over dropdown list options
    Then Make sure each dropdown window has blue highlight on hover
