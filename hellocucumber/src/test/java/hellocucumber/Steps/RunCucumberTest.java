package hellocucumber.Steps;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = "pretty", features = "/Users/marina/Documents/QA/cucumber/hellocucumber/src/test/java/hellocucumber/Features")
public class RunCucumberTest {
}
