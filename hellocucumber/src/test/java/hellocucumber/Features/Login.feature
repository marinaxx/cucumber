Feature: Login Feature
  This login feature deals with login functionality of the namecheap website.

  Scenario: Make sure the support/sign in/sign up header bar is present on the home page
    Given Navigate to the web page
    Then Locate header bar

  Scenario Outline: I Login using variation of login details
    Given Navigate to the web page Sign In
    And Enter <username> and <password>
    And Click Sign In
    Then User page should load

    Examples:
      | username | password     |
#      | new      | user         |
      | Aphness  | Orange7Juice |

