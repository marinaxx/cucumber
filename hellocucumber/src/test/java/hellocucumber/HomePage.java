package hellocucumber;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//button[@class='gb-btn gb-btn--link gb-dropdown__toggle']")
    private WebElement signInDropdown;

    @FindBy(xpath = "//input[@name='LoginUserName']")
    private WebElement login;

    @FindBy(xpath = "//input[@name='LoginPassword']")
    private WebElement passworD;

    @FindBy(xpath = "//button[@class='gb-btn gb-btn--block gb-btn--secondary gb-btn--lg gb-mb-2']/span")
    private WebElement signin;


    public HomePage inputLoginDetails(String username, String password) {
        signInDropdown.click();
        login.sendKeys(username);
        passworD.sendKeys(password);
        return this;
    }

    public HomePage signIn() {
        signin.click();
        return this;
    }
}
